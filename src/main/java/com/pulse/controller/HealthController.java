package com.pulse.controller;

import java.util.List;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.pulse.bean.Health;
import com.pulse.bean.HealthPii;
import com.pulse.service.HealthService;

@Validated
@RestController
@RequestMapping(value = "/health")
public class HealthController {

  @Autowired
  HealthService healthService;

  @GetMapping(value = "/piiData")
  public ResponseEntity<List<HealthPii>> getPiiData(
      @RequestParam(name = "page", defaultValue = "0") 
        @Min(value = 0, message = "must greater than 0") int page,
      @RequestParam(name = "size", defaultValue = "10") 
        @Min(value = 10, message = "must greater than 10") 
        @Max(value = 50, message = "must less than 50") int size) {
    List<HealthPii> list = healthService.getPiiData(page, size);
    return new ResponseEntity<>(list, HttpStatus.OK);
  }

  @GetMapping(value = "/data")
  public ResponseEntity<List<Health>> getData(
      @RequestParam(name = "page", defaultValue = "0") 
        @Min(value = 0, message = "must greater than 0") int page,
      @RequestParam(name = "size", defaultValue = "10") 
        @Min(value = 10, message = "must greater than 10") 
        @Max(value = 50, message = "must less than 50") int size) {
    List<Health> list = healthService.getData(page, size);
    return new ResponseEntity<>(list, HttpStatus.OK);
  }
}
