package com.pulse.kafka;

import java.time.LocalDateTime;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.MapType;
import com.pulse.bean.Health;
import com.pulse.bean.HealthPii;
import com.pulse.dao.HealthDao;
import com.pulse.dao.HealthPiiDao;

@Component
public class DataConsumer {

  private static final Logger logger = LoggerFactory.getLogger(DataConsumer.class);

  private static final String CONTAINER_FACTORY = "kafkaListenerContainerFactory";

  @Autowired
  private HealthPiiDao healthPiiDao;

  @Autowired
  private HealthDao healthDao;

  private final ObjectMapper mapper = new ObjectMapper();
  private final MapType type =
      mapper.getTypeFactory().constructMapType(Map.class, String.class, Object.class);

  // @KafkaListener(groupId = "${spring.kafka.consumer.group-id}",
  // containerFactory = CONTAINER_FACTORY,
  // topicPartitions = {@TopicPartition(topic = "${data.kafka.topics.healthPii}",
  // partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))})
  @KafkaListener(topics = "${data.kafka.topics.healthPii}",
      groupId = "${spring.kafka.consumer.group-id}", containerFactory = CONTAINER_FACTORY)
  public void listenHeathPii(ConsumerRecord<String, String> event) {
    Map<String, String> data = null;
    try {
      data = mapper.readValue(event.value(), type);
    } catch (JsonProcessingException e) {
      logger.error("topic {} / data parsing failed: {}", event.topic(), event.value());
    }
    HealthPii healthPii = new HealthPii();
    healthPii.setEvent(event.key());
    healthPii.setTimestamp(event.timestamp());
    healthPii.setData(data);
    healthPii.setTtl(LocalDateTime.now());
    healthPiiDao.save(healthPii);
  }

  // @KafkaListener(groupId = "${spring.kafka.consumer.group-id}",
  // containerFactory = CONTAINER_FACTORY,
  // topicPartitions = {@TopicPartition(topic = "${data.kafka.topics.health}",
  // partitionOffsets = @PartitionOffset(partition = "0", initialOffset = "0"))})
  @KafkaListener(topics = "${data.kafka.topics.health}",
      groupId = "${spring.kafka.consumer.group-id}", containerFactory = CONTAINER_FACTORY)
  public void listenHeath(ConsumerRecord<String, String> event) {
    Map<String, String> data = null;
    try {
      data = mapper.readValue(event.value(), type);
    } catch (JsonProcessingException e) {
      logger.error("topic {} / data parsing failed: {}", event.topic(), event.value());
    }
    Health health = new Health();
    health.setEvent(event.key());
    health.setTimestamp(event.timestamp());
    health.setData(data);
    health.setTtl(LocalDateTime.now());
    healthDao.save(health);
  }
}
