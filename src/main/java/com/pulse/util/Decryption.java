package com.pulse.util;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Decryption {

  private static final String KEY_SPEC = "AES";
  private static final String CIPHER_METHOD = "AES/CBC/PKCS5Padding";
  private final Cipher cipher;
  private final byte[] key;
  private final int ivSize;
  
  @Autowired
  public Decryption(@Value("${data.decrypt.key}") String decryptKey) throws NoSuchAlgorithmException, NoSuchPaddingException {
    cipher = Cipher.getInstance(CIPHER_METHOD);
    key = Base64.getDecoder().decode(decryptKey);
    ivSize = cipher.getBlockSize();
  }

  public String encrypt(String inputString)
      throws InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException,
      BadPaddingException, ShortBufferException {
    
    SecretKeySpec privateKeySpec = new SecretKeySpec(key, KEY_SPEC);

    byte[] data = inputString.getBytes(StandardCharsets.UTF_8);
    byte[] iv = new byte[ivSize];
    SecureRandom secureRandom = new SecureRandom();
    secureRandom.nextBytes(iv);
    IvParameterSpec ivSpec = new IvParameterSpec(iv);

    cipher.init(Cipher.ENCRYPT_MODE, privateKeySpec, ivSpec);
    byte[] cipherBytes = new byte[iv.length + cipher.getOutputSize(data.length)];
    System.arraycopy(iv, 0, cipherBytes, 0, ivSize);
    cipher.doFinal(data, 0, data.length, cipherBytes, ivSize);
    return Base64.getEncoder().encodeToString(cipherBytes);
  }

  public String decrypt(String cipherTextBase64) throws InvalidKeyException,
      InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

    SecretKeySpec privateKeySpec = new SecretKeySpec(key, KEY_SPEC);
    
    byte[] data = Base64.getDecoder().decode(cipherTextBase64);
    byte[] iv = Arrays.copyOfRange(data, 0, ivSize);
    IvParameterSpec ivSpec = new IvParameterSpec(iv);

    cipher.init(Cipher.DECRYPT_MODE, privateKeySpec, ivSpec);
    byte[] finalBytes = cipher.doFinal(data, ivSize, data.length - ivSize);
    return new String(finalBytes);
  }
}
