package com.pulse.service;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.util.List;
import java.util.Map;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.pulse.bean.Health;
import com.pulse.bean.HealthPii;
import com.pulse.dao.HealthDao;
import com.pulse.dao.HealthPiiDao;
import com.pulse.util.Decryption;

@Service
public class HealthService {
  
  private static final Logger logger = LoggerFactory.getLogger(HealthService.class);

  @Autowired
  private HealthDao healthDao;
  
  @Autowired
  private HealthPiiDao healthPiiDao;
  
  @Autowired
  private Decryption decryption;

  public List<Health> getData(int page, int size) {
    
    logger.info("page {}, size {}", page, size);
    
    final Pageable pageableRequest = PageRequest.of(page, size);
    Page<Health> pages =  healthDao.findAll(pageableRequest);
    return pages.getContent();
  }
  
  public List<HealthPii> getPiiData(int page, int size) {
    
    logger.info("page {}, size {}", page, size);
    
    final Pageable pageableRequest = PageRequest.of(page, size);
    Page<HealthPii> pages =  healthPiiDao.findAll(pageableRequest);
    List<HealthPii> list = pages.getContent();
    
    for(HealthPii healthPii: list) {
      Map<String, String> data = healthPii.getData();
      
      for(Map.Entry<String, String> kv: data.entrySet()) {
        String key = kv.getKey();
        String encryptVal = kv.getValue();
        try {
          String decryptVal = decryption.decrypt(encryptVal);
          data.put(key, decryptVal);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException
            | IllegalBlockSizeException | BadPaddingException e) {
          logger.error("error parsing key {}", key);
        }
      }
    }    
    return list;
  }
}
