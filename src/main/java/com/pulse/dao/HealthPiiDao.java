package com.pulse.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.pulse.bean.HealthPii;

public interface HealthPiiDao extends MongoRepository<HealthPii, String>  {

}