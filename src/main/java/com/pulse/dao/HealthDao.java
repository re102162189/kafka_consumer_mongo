package com.pulse.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.pulse.bean.Health;

public interface HealthDao extends MongoRepository<Health, String>  {

}