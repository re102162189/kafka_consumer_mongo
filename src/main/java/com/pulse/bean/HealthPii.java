package com.pulse.bean;

import java.util.Map;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "healthPii")
public class HealthPii extends Base {

  private Map<String, String> data;

  public Map<String, String> getData() {
    return data;
  }

  public void setData(Map<String, String> data) {
    this.data = data;
  }
}
