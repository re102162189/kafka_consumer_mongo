package com.pulse.bean;

import java.time.LocalDateTime;
import org.springframework.data.mongodb.core.index.Indexed;

public abstract class Base {
  
  private String event;
  
  private long timestamp;
  
  @Indexed(name = "ttl", expireAfter = "90d" )
  private LocalDateTime ttl;
  
  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public long getTimestamp() {
    return timestamp;
  }
  
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  public LocalDateTime getTtl() {
    return ttl;
  }

  public void setTtl(LocalDateTime ttl) {
    this.ttl = ttl;
  }
}
