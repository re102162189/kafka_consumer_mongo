## build/start all containers

```
# check loki (promtail), prometheus and tempo config
#
$ docker-compose build
$ docker-compose up -d
```

---

## Kafka UI / MongoDB UI

1. https://www.confluent.io/product/confluent-platform/gui-driven-management-and-monitoring/
2. https://www.mongodb.com/products/compass


---

## start/stop/restart/remove containers

```
$ docker-compose start
$ docker-compose stop
$ docker-compose restart
$ docker-compose down
```

---

## remove docker volume & network

```
$ docker volume ls
$ docker volume rm {volume_id}

$ docker network ls
$ docker network rm {network_id}
```